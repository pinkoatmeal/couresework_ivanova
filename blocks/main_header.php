<header class="main-head">
    <div class="head-reglog-box">
        <div>
            <?php

            if (empty($_SESSION['username']) or empty($_SESSION['user_id']))
            {
                echo "
                <p class='reglog-box-text'>
                    <a href='login.php' class='reglog-a'>Вход</a> | <a href='../pages/registration_user.php' class='reglog-a'>Регистарция</a>
                </p>
                <p style='text-align: center'>
                    <a href='../pages/create_deal_guest.php' class='reglog-a'>Получить помощь от нотариуса</a>
                </p>
                ";
            }

            else
            {
                echo "<p class='reglog-box-text'>Здравствуйте, ".$_SESSION['username']."</p>";
                echo "
                <p style='text-align: center'><a href='../handlers/out.php' class='reglog-a'>Выход</a> | ";
                if ($_SESSION["notary_flag"])
                {
                    echo "<a href='../pages/notary_deal_table.php' class='reglog-a'>Мои заказы</a>";
                }
                elseif ($_SESSION["admin_flag"])
                {
                    echo "<a href='../pages/admin_deal_table.php' class='reglog-a'>Заказы</a>";
                }
                else
                {
                    echo "<a href='../pages/client_deal_table.php' class='reglog-a'>Мои Заказы</a> | ";
                    echo "<a href='../pages/create_deal_user.php' class='reglog-a'>Новый заказ</a></p>";
                }
            }

            ?>
        </div>
    </div>
    <div class="head-text-block">
        <a href="../pages/index.php" style="text-decoration: none"><h1 class="head-text">Нотариусы Города</h1></a>
    </div>
</header>
