<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Главная</title>
    <link href="../frontend/style.css" type="text/css" rel="stylesheet">
</head>
<body>
<?php include("../blocks/main_header.php"); ?>
<div class="flex">
    <nav class="menu" style="background-color: white; border-radius: 3px; margin: 0 0 0 100px;">
        <h3 style="text-align: center">Меню</h3>
        <ul style="margin: 10px 0 5px; list-style-type: none;">
            <a href="our_notaries_table.php" class="reglog-a"><li style="padding: 0 25px 5px 10px">Наши нотариусы</li></a>
            <a href="../pages/service_table.php" class="reglog-a"><li style="padding: 0 25px 5px 10px" href="#">Услуги</li></a>
        </ul>
    </nav>
    <div class="content_box">
        <?php
        if (empty($_SESSION['username']) or empty($_SESSION['user_id']))
        {
            exit("Эта страница доступна только авторизированным пользователям");
        }
        ?>
        <div style="background-color: white; margin-left: auto; margin-right: auto; width:">
            <table>
                <tr>
                    <th>Название компании</th>
                    <th>Номер телефона</th>
                    <th>Название услуги</th>
                    <th>Коммиссия</th>
                </tr>
                <?php

                $connection = new SQLite3("C:\Folder\\xampp\htdocs\\notaries\data\\notarial_office.db");

                $result = $connection->query("SELECT cli.company_name, cli.phone, ser.service_name, ser.commission FROM Clients cli INNER JOIN Services ser INNER JOIN Deals dl ON dl.id_service = ser.id_service AND dl.id_client = cli.id_client WHERE id_notary NOTNULL");

                while ($row = $result->fetchArray())
                {
                    echo "<tr>";
                    echo "<td>" . $row["company_name"] . "</td>";
                    echo "<td>" . $row["phone"] . "</td>";
                    echo "<td>" . $row["service_name"] . "</td>";
                    $total_price = $row["commission"];
                    echo "<td>" . (string)$total_price . "</td>";
                    echo "</tr>";
                }

                $connection->close();

                ?>
            </table>
            <?php

            $connection = new SQLite3("C:\Folder\\xampp\htdocs\\notaries\data\\notarial_office.db");

            $user_id = $_SESSION["user_id"];

            $result = $connection->query("SELECT sum(commission * 0.1) FROM Clients cli INNER JOIN Services ser INNER JOIN Deals dl ON dl.id_service = ser.id_service AND dl.id_client = cli.id_client WHERE id_notary NOTNULL")->fetchArray();

            echo "<hr>";
            echo "<p style='padding: 10px 10px; font-weight: bold'>Ваш итоговый заработок: ".$result[0]."</p>";

            $result = $connection->query("SELECT avg(commission * 0.1) FROM (SELECT commission FROM Deals dl INNER JOIN Services ser ON ser.id_service = dl.id_service ORDER BY dl.id_deal DESC LIMIT 0, 3)")->fetchArray();

            echo "<p style='padding: 10px 10px; font-weight: bold'>Средний за последние три заказа: ".(int)$result[0]."</p>";

            $connection->close();

            ?>
        </div>
    </div>
</div>
</body>
</html>