<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Регистарция</title>
    <link rel="stylesheet" media="screen" href="../frontend/style.css">
</head>
<body>
<?php include("../blocks/mini_header.php"); ?>
<div>
    <h2 align="center" style="padding: 10px;">Регистрация</h2>
    <a href="regestration_notary.php" class="reglog-a"><p align="center">Вы нотариус?</p></a>
</div>
<div align="center">
    <form action="../handlers/reg_handler.php" method="post" class="deal_form">
        <ul>
            <li>
                <p class="required_notification">*</p>
                <label for="company_name">Название компании:</label>
                <input placeholder="" type="text" size="30" name="company_name" required>
            </li>
            <li>
                <p class="required_notification">*</p>
                <label for="phone">Номер телефона:</label>
                <input placeholder="+7xxxxxxxxxx" type="tel" size="30" name="phone" required pattern="\+7\-[0-9]{3}\-[0-9]{3}\-[0-9]{2}\-[0-9]{2}">
            </li>
            <li>
                <p class="required_notification">*</p>
                <label for="password">Пароль: </label>
                <input placeholder="" name="password" type="password" required>
            </li>
            <li>
                <input placeholder="Зарегистрироваться" type="submit">
            </li>
        </ul>
    </form>
    <p class="required_notification" style="margin: 0 625px 0 0;">* - Обязательное поле</p>
    <p>Уже есть аккаунт? <a href="login.php" class="reglog-a">Войти</a>?</p>
</div>
</body>
</html>