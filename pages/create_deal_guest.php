<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Создание сделки</title>
    <link href="../frontend/style.css" type="text/css" rel="stylesheet">
</head>
<body>
<?php include("../blocks/main_header.php"); ?>
<div style="margin: 10px 10px">
    <h2 style="margin: 20px 0">Заяавка на составление договора</h2>
    <p class="required_notification">* - Обязательное поле</p>
    <form action="../handlers/add_deal_toDB_guest.php" method="post" class="deal_form">
        <ul>
            <li>
                <p class="required_notification">*</p>
                <label for="company_name">Название компании:</label>
                <input placeholder="" type="text" size="30" name="company_name" required>
            </li>
            <li>
                <p class="required_notification">*</p>
                <label for="address">Адрес:</label>
                <input placeholder="г. ААА, ул. BBB, д. 000" type="text" size="30" name="address" required>
            </li>
            <li>
                <p class="required_notification">*</p>
                <label for="phone">Номер телефона:</label>
                <input placeholder="+7xxxxxxxxxx" type="tel" size="30" name="phone" required pattern="\+7\-[0-9]{3}\-[0-9]{3}\-[0-9]{2}\-[0-9]{2}">
            </li>
            <li>
                <p class="required_notification">*</p>
                <label>Услуга:</label>
                <select name="service_name" class="service_name_selector">
                    <?php

                    $connection = new SQLite3("C:\Folder\\xampp\htdocs\\notaries\data\\notarial_office.db");

                    $result = $connection->query("SELECT service_name FROM Services");

                    while($row = $result->fetchArray())
                    {
                        echo "<option>".$row["service_name"]."</option>";
                    }

                    $connection->close();

                    ?>
                </select>
            </li>
            <li>
                <p class="required_notification">*</p>
                <label for="work">Направление работы:</label>
                <textarea placeholder="" rows="5" cols="30" name="work" required></textarea>
            </li>
            <li>
                <p class="required_notification">*</p>
                <label for="deal_description">Описание сделки:</label>
                <textarea placeholder="" rows="5" cols="30" name="deal_description" required></textarea>
            </li>
            <li>
                <input placeholder="Отправить" type="submit">
            </li>
        </ul>
    </form>
</div>
</body>
</html>