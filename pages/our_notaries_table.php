<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Главная</title>
    <link href="../frontend/style.css" type="text/css" rel="stylesheet">
</head>
<body>
<?php include("../blocks/main_header.php"); ?>
<div class="flex">
    <nav class="menu" style="background-color: white; border-radius: 3px; margin: 0 0 0 100px;">
        <h3 style="text-align: center">Меню</h3>
        <ul style="margin: 10px 0 5px; list-style-type: none;">
            <a href="#" class="reglog-a"><li style="padding: 0 25px 5px 10px">Наши нотариусы</li></a>
            <a href="../pages/service_table.php" class="reglog-a"><li style="padding: 0 25px 5px 10px">Услуги</li></a>
        </ul>
    </nav>
    <div class="content_box">
        <div style="background-color: white; margin-left: auto; margin-right: auto; width:">
            <table>
                <tr>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Стаж</th>
                </tr>
                <?php

                $connection = new SQLite3("C:\Folder\\xampp\htdocs\\notaries\data\\notarial_office.db");

                $result = $connection->query("SELECT name, surname, experience FROM Notaries");

                while ($row = $result->fetchArray())
                {
                    echo "<tr>";
                    echo "<td>" . $row["name"] . "</td>";
                    echo "<td>" . $row["surname"] . "</td>";
                    echo "<td>" . $row["experience"]. " лет" . "</td>";
                    echo "</tr>";
                }

                $connection->close();

                ?>
            </table>
        </div>
    </div>
</div>
</body>
</html>