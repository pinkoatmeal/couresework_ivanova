<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Вход</title>
    <link rel="stylesheet" media="screen" href="../frontend/style.css">
</head>
<body>
<?php include("../blocks/mini_header.php"); ?>
<div>
    <h2 align="center" style="padding: 10px;">Вход</h2>
</div>
<div align="center">
    <form action="../handlers/login_handler.php" method="post" class="deal_form">
        <ul>
            <li>
                <label for="phone">Номер телефона:</label>
                <input placeholder="+7xxxxxxxxxx" type="tel" size="30" name="loginPhone" required minlength="12" maxlength="12" pattern="\+7\-[0-9]{3}\-[0-9]{3}\-[0-9]{2}\-[0-9]{2}">
            </li>
            <li>
                <label for="password">Пароль:</label>
                <input placeholder="" name="password" type="password" required>
            </li>
            <li>
                <input placeholder="Войти" type="submit">
            </li>
        </ul>
        <p>Нет аккаунта? <a href="registration_user.php" class="reglog-a">Зарегистрироваться</a>?</p>
    </form>
</div>
</body>
</html>