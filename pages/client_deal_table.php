<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Главная</title>
    <link href="../frontend/style.css" type="text/css" rel="stylesheet">
</head>
<body>
<?php include("../blocks/main_header.php"); ?>
<div class="flex">
    <nav class="menu" style="background-color: white; border-radius: 3px; margin: 0 0 0 100px;">
        <h3 style="text-align: center">Меню</h3>
        <ul style="margin: 10px 0 5px; list-style-type: none;">
            <a href="our_notaries_table.php" class="reglog-a"><li style="padding: 0 25px 5px 10px">Наши нотариусы</li></a>
            <a href="../pages/service_table.php" class="reglog-a"><li style="padding: 0 25px 5px 10px" href="#">Услуги</li></a>
        </ul>
    </nav>
    <div class="content_box">
        <?php
        if (empty($_SESSION['username']) or empty($_SESSION['user_id']) or $_SESSION["notary_flag"] or $_SESSION["admin_flag"])
        {
            exit("Эта страница доступна только авторизированным пользователям");
        }
        ?>
            <div style="background-color: white; margin-left: auto; margin-right: auto; width:">
            <table>
                <tr>
                    <th>Название услуги</th>
                    <th>Описание сделки</th>
                    <th>Итого</th>
                </tr>
                <?php

                $connection = new SQLite3("C:\Folder\\xampp\htdocs\\notaries\data\\notarial_office.db");

                $client_id = $_SESSION["user_id"];

                $result = $connection->query("SELECT ser.id_service, ser.service_name, ser.total, ser.commission, dl.deal_description FROM Services ser INNER JOIN Deals dl ON ser.id_service = dl.id_service AND dl.id_client = $client_id");

                while ($row = $result->fetchArray())
                {
                    echo "<tr>";
                    echo "<td>" . $row["service_name"] . "</td>";
                    echo "<td>" . $row["deal_description"] . "</td>";
                    $total_price = $row["total"] + $row["commission"];
                    echo "<td>" . (string)$total_price . "</td>";
                    echo "</tr>";
                }

                $connection->close();

                ?>
            </table>
        </div>
    </div>
</div>
</body>
</html>