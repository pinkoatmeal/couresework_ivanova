<?php session_start(); ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Главная</title>
    <link href="../frontend/style.css" type="text/css" rel="stylesheet">
</head>
<body>
<?php include("../blocks/main_header.php"); ?>
<div class="flex">
    <nav class="menu" style="background-color: white; border-radius: 3px; margin: 0 0 0 100px;">
        <h3 style="text-align: center">Меню</h3>
        <ul style="margin: 10px 0 5px; list-style-type: none;">
            <a href="our_notaries_table.php" class="reglog-a"><li style="padding: 0 25px 5px 10px">Наши нотариусы</li></a>
            <a href="../pages/service_table.php" class="reglog-a"><li style="padding: 0 25px 5px 10px" href="#">Услуги</li></a>
        </ul>
    </nav>
    <div class="content_box">
        <?php
        if (empty($_SESSION['username']) or empty($_SESSION['user_id']))
        {
            exit("Эта страница доступна только авторизированным пользователям");
        }
        ?>
        <div style="background-color: white; margin-left: auto; margin-right: auto; width:">
            <table>
                <tr>
                    <th>Название компании</th>
                    <th>Номер телефона</th>
                    <th>Название услуги</th>
                    <th>Описание сделки</th>
                    <th>Коммиссия</th>
                </tr>
                <?php

                $connection = new SQLite3("C:\Folder\\xampp\htdocs\\notaries\data\\notarial_office.db");

                $user_id = $_SESSION["user_id"];

                $result = $connection->query("SELECT ser.id_service, ser.service_name, ser.commission, dl.deal_description, cli.company_name, cli.phone FROM Services ser INNER JOIN Deals dl ON ser.id_service = dl.id_service AND dl.id_notary = $user_id INNER JOIN Clients cli ON dl.id_client = cli.id_client");

                while ($row = $result->fetchArray())
                {
                    echo "<tr>";
                    echo "<td>" . $row["company_name"] . "</td>";
                    echo "<td>" . $row["phone"] . "</td>";
                    echo "<td>" . $row["service_name"] . "</td>";
                    echo "<td>" . $row["deal_description"] . "</td>";
                    $total_price = $row["commission"];
                    echo "<td>" . (string)$total_price . "</td>";
                    echo "</tr>";
                }

                $connection->close();

                ?>
            </table>
            <?php

            $connection = new SQLite3("C:\Folder\\xampp\htdocs\\notaries\data\\notarial_office.db");

            $user_id = $_SESSION["user_id"];

            $result = $connection->query("SELECT sum(commission * 0.9) FROM Services ser INNER JOIN Deals dl ON ser.id_service = dl.id_service AND dl.id_notary = $user_id")->fetchArray();

            echo "<hr>";
            echo "<p style='padding: 10px 10px; font-weight: bold'>Ваш итоговый заработок: ".$result[0]."</p>";

            $connection->close();

            ?>
        </div>
    </div>
</div>
</body>
</html>