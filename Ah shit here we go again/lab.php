<?php session_start();?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Клиенты</title>
</head>
<body>
<table border="1">
    <tr>
        <th>Клиент</th>
        <th>Услуга</th>
        <th>Описание сделки</th>
    </tr>
    <?php
    $connection = new SQLite3("C:\Folder\\xampp\htdocs\Web_course_work\data\\notarial_office.db");

    $result = $connection->query("SELECT company_name, phone, address FROM Clients");
    while ($row = $result->fetchArray())
    {
        echo "<tr>";
        echo "<td>".$row["company_name"]."</td>";
        echo "<td>".$row["phone"]."</td>";
        echo "<td>".$row["address"]."</td>";
        echo "</tr>";
    }

    $connection->close()
    ?>
</table>
</body>
</html>
