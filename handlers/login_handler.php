<?php

session_start();

if (isset($_POST['loginPhone']))
{
    $loginPhone = $_POST['loginPhone'];
    if ($loginPhone == '')
    {
        unset($loginPhone);
    }
}

if (isset($_POST['password']))
{
    $password=$_POST['password'];
    if ($password =='')
    {
        unset($password);
    }
}

if (empty($_POST["loginPhone"]) or empty($_POST["password"]))
{
    exit("Вы ввели не всю информацию, вернитесь назад и заполните все поля!");
}

$connection = new SQLite3("C:\Folder\\xampp\htdocs\\notaries\data\\notarial_office.db");

$result = $connection->query("SELECT id_client, company_name, phone, password FROM Clients WHERE phone = $loginPhone")->fetchArray();
$user = true;
$admin = false;
$notary = false;


if (empty($result))
{
    $result = $connection->query("SELECT id_notary, name, password FROM Notaries WHERE phone = $loginPhone")->fetchArray();
    $notary = true;
    $user = false;
    $admin = false;
}

if (empty($result))
{
    $result = $connection->query("SELECT id_admin, name, password FROM Admins WHERE phone = $loginPhone")->fetchArray();
    $admin = true;
    $notary = false;
    $user = false;
}

if (empty($result["password"]))
{
    exit("Извините, введённый вами login или пароль неверный.");
}
if ($result["password"] == md5($password))
{
    if ($notary)
    {
        $_SESSION["username"] = $result["name"];
        $_SESSION["user_id"] = $result["id_notary"];
        $_SESSION["notary_flag"] = true;
        $_SESSION["admin_flag"] = false;
        $_SESSION["user_flag"] = false;
    }

    elseif ($admin)
    {
        $_SESSION["username"] = $result["name"];
        $_SESSION["user_id"] = $result["id_admin"];
        $_SESSION["admin_flag"] = true;
        $_SESSION["notary_flag"] = false;
        $_SESSION["user_flag"] = false;
    }

    else
    {
        $_SESSION["username"] = $result["company_name"];
        $_SESSION["user_id"] = $result["id_client"];
        $_SESSION["user_flag"] = true;
        $_SESSION["notary_flag"] = false;
        $_SESSION["admin_flag"] = false;
    }
    echo "Вы успешно вошли на сайт! <a href='../pages/index.php'>Главная страница</a>";
    header("Location: ../pages/index.php");
}
else
{
    exit("Извините, введённый вами login или пароль неверный.");
}

$connection->close();

?>